import {FormControl, InputLabel, ListItem, ListItemIcon, ListItemText, Select} from "@mui/material";
import {Filter} from "@/types/filters/Filter";
import MenuItem from "@mui/material/MenuItem";
import {AvailabilityFilter} from "@/types/filters/AvailabilityFilter";
import {DateTimePicker, LocalizationProvider, MobileDateTimePicker} from "@mui/x-date-pickers";
import {AdapterDayjs} from "@mui/x-date-pickers/AdapterDayjs";
import {Dayjs} from "dayjs";
import Image from "next/image";
import * as React from "react";
import {csCZ} from "@mui/x-data-grid";
import csLocale from "@fullcalendar/core/locales/cs";
import 'dayjs/locale/cs';
import {SortField} from "@/types/filters/Sort";
import Button from "@mui/material/Button";
import {FiltersFactory} from "@/types/filters/FiltersFactory";
import {DateTimeValidationError} from "@mui/x-date-pickers";

export default function FilterBar({filters, onChange, onReset}: { filters: Filter[], onChange: (newFilter: Filter) => void , onReset: () => void}) {
    const styles = {
        menuItem: {
            height: '48px', // Set the desired height
            display: 'flex',
            alignItems: 'center',
        },
    };
    return (
        <div style={{display: 'flex', width: '100%', justifyContent: 'space-around'}}>
            {filters.map((filter, key) => {
                if (filter instanceof AvailabilityFilter) {
                    return <FormControl key={key} variant="standard" sx={{m: 1, minWidth: 120, flex: 1}}>
                        <LocalizationProvider dateAdapter={AdapterDayjs} adapterLocale="cs">
                            <DateTimePicker
                                label={filter.filterName()}
                                value={filter.getCurrentChoice().key}
                                defaultValue={undefined}
                                onChange={(newValue) => onChange(filter.copy((newValue as Dayjs)?.toDate()))}
                                onError={(error, value) => {
                                    if (error == 'invalidDate') {
                                        onChange(filter.copy(null))
                                    }
                                }}
                                slotProps={{
                                    actionBar: {
                                        actions: ['clear', 'accept'],
                                    },
                                }}
                            />
                        </LocalizationProvider>
                    </FormControl>
                }

                return <FormControl key={key}  sx={{m: 1, minWidth: 120, flex: 1}} >
                    <InputLabel>{filter.filterName()}</InputLabel>

                    <Select
                        value={filter.getCurrentChoice().key}
                        onChange={(event) => onChange(filter.copy(event.target.value))}
                    >
                        <MenuItem value=''>-</MenuItem>
                        {filter.getChoices().map((f, k) => (
                            <MenuItem key={k} value={f.key} >
                                <ListItem>
                                    {f.value.icon_src !== undefined &&
                                        <ListItemIcon><Image src={f.value.icon_src} alt={f.value.name} title={f.value.name} height={20}/></ListItemIcon>}
                                    <ListItemText>{f.value.name}</ListItemText>
                                </ListItem>
                            </MenuItem>
                        ))}
                    </Select>
                </FormControl>
            })}

            <Button variant="text" onClick={onReset}>Zrušit filtry</Button>
        </div>
    )
}