import {Sort} from "@/types/filters/Sort";
import {AvailabilityFilter} from "@/types/filters/AvailabilityFilter";
import {SizeFilter} from "@/types/filters/SizeFilter";
import {SuitabilityFilter} from "@/types/filters/SuitabilityFilter";
import {NeedForAttentionFilter} from "@/types/filters/NeedForAttentionFilter";


export class FiltersFactory {
    static getAllFilters() {
        return [
            new Sort(),
            new AvailabilityFilter(),
            new SizeFilter(),
            new SuitabilityFilter(),
            new NeedForAttentionFilter()
        ]
    }
}