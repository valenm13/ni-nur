'use client'

import Typography from "@mui/material/Typography";
import React, {useEffect, useState} from 'react';

import { useRouter } from "next/navigation";
import ReservationForm from "@/components/ReservationForm";
import IconButton from "@mui/material/IconButton";
import ArrowBackIcon from "@mui/icons-material/ArrowBack"

const events = [
    {title: 'Meeting', start: new Date()}
]

export default function ReservationPage() {
    const router = useRouter();
    // Check if window is defined before using it
    const [dogName, setDogName] = useState<string>("null");

    useEffect(() => {
        // Check if window is available (client-side rendering)
        if (typeof window !== 'undefined') {
            let params = new URLSearchParams(window.location.search);
            setDogName(params && params.get('dog') || "");
        }
    }, []);
    const handleBack = () => {
        router.back()
    };

    return <div>
        <div className="row"><IconButton onClick={handleBack} color="primary">
            <ArrowBackIcon/>
        </IconButton>
            <Typography variant="h3">Rezervovat pejska pro venčení</Typography></div>
        <ReservationForm dogName={dogName} id={""} isEdit={false}/>
    </div>

}



